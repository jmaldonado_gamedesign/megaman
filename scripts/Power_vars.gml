enum Power_states {

    normal,
    teleporter,
    acrobatic,
    shooter,

}
actual_state = Power_states.normal;
is_normal_state = true;
is_teleporter_state = false;
is_acrobatic_state = false;
is_shooter_state = false;

//the mana bar
global.mana_num = 100;

//the direction for the power sprites in draw
set_movement = 1;

