var jumpreverse = false;
//if the mana depletes
if(global.mana_num <= 0){ 
    actual_state = Power_states.normal; 
    audio_stop_sound(sd_levitate);
    effect_create_below(ef_smokeup, x, y, 0, c_black);
}


switch(actual_state){

    case Power_states.normal: //normal movement
        is_normal_state = true;
        is_teleporter_state = false;
        is_acrobatic_state = false;
        is_shooter_state = false;
        hsp = move * movespeed;
        if(place_meeting(x, y+1, obj_ground)){
             if(key_jump){ vsp = -jumpspeed; audio_play_sound(s_jump,10,0);}
        } 
        break;
    case Power_states.teleporter:
        key_jump = keyboard_check(vk_space);
        is_normal_state = false;
        is_teleporter_state = true;
        is_acrobatic_state = false;
        is_shooter_state = false;
        hsp = move * movespeed;
        if(key_jump){ 
        vsp -= 3; 
        global.mana_num -= 1.3;
        audio_play_sound(sd_levitate, 10, true); 
        }
        else audio_stop_sound(sd_levitate);
        break;
        
    
    case Power_states.acrobatic:
        is_normal_state = false;
        is_teleporter_state = false;
        is_acrobatic_state = true;
        is_shooter_state = false;
        jumpspeed = 25;
        hsp = move * movespeed;
        if(place_meeting(x, y+1, obj_ground)){
             if(key_jump){ 
             vsp = -jumpspeed; 
             audio_play_sound(s_jump,10,0); global.mana_num -= 3;
             }
             
        }
        else if ((key_jump) && (place_meeting(x + 1,y, obj_ground) || place_meeting(x - 1,y, obj_ground))){
            jumpreverse = true;  
            global.mana_num -= 10;  
        }        
        break;
    case Power_states.shooter:
         is_normal_state = false;
        is_teleporter_state = false;
        is_acrobatic_state = false;
        is_shooter_state = true;
        hsp = move * movespeed;
        //we create a bullet
        if(key_jump){ 
            audio_play_sound(sd_shoot, 10, false); 
            var bullet = instance_create (x,y,obj_bullet);
            if(set_movement < 0){ bullet.speed = -9; bullet.image_xscale = -1}
            else if(set_movement > 0){ bullet.speed = 9;}
           
          global.mana_num -= 20; 
        }
        break;

}

if(is_acrobatic_state == false){
    jumpspeed = 20;
}

//changing jump when he collides in a wall
if(jumpreverse == true && !place_meeting(x, y+1, obj_ground)){
    if place_meeting(x + 1,y, obj_ground) movejump = -1;
    if place_meeting(x - 1,y, obj_ground) movejump = 1;
    hsp = movejump * movespeed;
    vsp -= jumpspeed;
}
else if(jumpreverse == true && place_meeting(x, y+1, obj_ground)){
    jumpreverse = false;
}


